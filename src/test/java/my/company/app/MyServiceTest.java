package my.company.app;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import my.company.app.lib.MyService;

public class MyServiceTest {
 
    /**
     * Rigorous Test Once Again :-)
     */
    @Test
    public void addTwoNaturalNumbers()
    {
        final MyService service = new MyService();
        assertEquals(2, service.doAdd(1, 1));
    }
}
